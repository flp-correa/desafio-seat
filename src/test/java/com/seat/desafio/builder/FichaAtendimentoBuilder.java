package com.seat.desafio.builder;

import java.util.ArrayList;

import com.seat.desafio.model.Atendimento;
import com.seat.desafio.model.FichaAtendimento;

public class FichaAtendimentoBuilder {

	private FichaAtendimento instancia;
	
	public FichaAtendimentoBuilder() {
		instancia = new FichaAtendimento();
		instancia.setAtendimentos(new ArrayList<>());
	}
	
	public FichaAtendimentoBuilder comAtendimentos(Atendimento atendimento) {
		instancia.getAtendimentos().add(atendimento);
		return this;
	}
	
	public FichaAtendimento construir() {
		return instancia;
	}
}
