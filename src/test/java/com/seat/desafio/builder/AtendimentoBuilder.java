package com.seat.desafio.builder;

import com.seat.desafio.model.Atendimento;
import com.seat.desafio.model.PrioridadeAtendimento;

public class AtendimentoBuilder {

	private Atendimento instancia;
	
	public AtendimentoBuilder() {
		instancia = new Atendimento();
	}
	
	public AtendimentoBuilder comSenha(String senha) {
		instancia.setSenha(senha);
		return this;
	}

	public AtendimentoBuilder comEmissao(Long emissao) {
		instancia.setEmissao(emissao);
		return this;
	}

	public AtendimentoBuilder comPrioridade(PrioridadeAtendimento prioridade) {
		instancia.setPrioridade(prioridade);
		return this;
	}

	public AtendimentoBuilder comChamada(Long chamada) {
		instancia.setChamada(chamada);
		return this;
	}

	public Atendimento construir() {
		return instancia;
	}
}
