package com.seat.desafio.service.FichaAtendimento;

import org.junit.Before;

import com.seat.desafio.model.FichaAtendimento;
import com.seat.desafio.service.fichaAtendimento.FichaAtendimentoService;
import com.seat.desafio.service.fichaAtendimento.xml.FichaAtendimentoXML;

public class FichaAtendimentoServiceTest {

	private FichaAtendimentoService fichaAtendimentoServiceXML;
	private FichaAtendimento fichaAtendimento;
	
	@Before
	public void setup() {
		fichaAtendimentoServiceXML = new FichaAtendimentoXML();
		fichaAtendimento = fichaAtendimentoServiceXML.buscarFichaDeAtendimento("151799685.xml");
	}
	
		/*
	@Test
	public void deveEliminarFichasDeAtendimentoDuplicadas() throws Exception {
		fichaAtendimentoServiceXML.removerFichasDiplicadas(fichaAtendimento);
		assertEquals(27, fichaAtendimento.getAtendimentos().size());
	}
	
	@Test
	public void listaDeAtendimentoDeveEstaOrenadaPorEmissao() throws Exception {
		fichaAtendimento.setAtendimentos(fichaAtendimentoServiceXML.ordernarFichaAtendimentoPorEmissao(fichaAtendimento.getAtendimentos()));
		assertEquals( (long)1517861290000L, (long) fichaAtendimento.getAtendimentos().get(0).getEmissao());
	}
	
	@Test
	public void deveRemoverAtendidosListaDeResultadosParaCalculoDeTempoDeEspera() throws Exception {
		fichaAtendimento.setResultados(fichaAtendimentoServiceXML.ordernarFichaAtendimentoPorEmissao(fichaAtendimento.getAtendimentos()));
		fichaAtendimentoServiceXML.removerAtendidosParaResultadoFinal(fichaAtendimento.getResultados());
		assertEquals(13, fichaAtendimento.getResultados().size());
	}
	
	@Test
	public void deveOrdenarListaDeResultadosDeEsperaPorPrioridadeEEmissao() throws Exception {
		fichaAtendimento.getResultados().addAll(fichaAtendimentoServiceXML.ordernarFichaAtendimentoPorEmissao(fichaAtendimento.getAtendimentos()));
		fichaAtendimentoServiceXML.removerAtendidosParaResultadoFinal(fichaAtendimento.getResultados());
		fichaAtendimentoServiceXML.ordenarListaDeEsperaPorPrioridadeEEmissao(fichaAtendimento.getResultados());
		assertEquals("E 216", fichaAtendimento.getResultados().get(0).getSenha());
	}*/
}
