package com.seat.desafio.repository;

import com.seat.desafio.model.FichaAtendimento;

public interface FichasAtendimento {

	public FichaAtendimento buscarFichaDeAtendimento(String nome);
	public String enviarFichasAtendimento(FichaAtendimento fichaAtendimento);
}
