package com.seat.desafio.service.fichaAtendimento.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.seat.desafio.model.Atendimento;
import com.seat.desafio.model.FichaAtendimento;
import com.seat.desafio.repository.FichasAtendimento;
import com.thoughtworks.xstream.XStream;

public class FichasAtendimentoXML implements FichasAtendimento {
	
	@Value("${seat.xml-storage-local.local}")
	private Path local;

	@Override
	public FichaAtendimento buscarFichaDeAtendimento(String nome) {
		XStream xstream = new XStream();
	    xstream.alias("ficha-atendimento", FichaAtendimento.class);
	    xstream.alias("atendimentos", List.class);
	    xstream.alias("atendimento", Atendimento.class);
	    
	    return (FichaAtendimento) xstream.fromXML(this.getClass().getResource("/" + nome));
	}

	@Override
	public String enviarFichasAtendimento(FichaAtendimento fichaAtendimento) {
		XStream xstream = new XStream();
	    xstream.alias("ficha-atendimento", FichaAtendimento.class);
	    xstream.alias("atendimentos", List.class);
	    xstream.alias("atendimento", Atendimento.class);
	    
	    OutputStream out = null;
        try {
	        out = new FileOutputStream(fichaAtendimento.getChave() + ".xml");
	        xstream.toXML(fichaAtendimento, out);
	        return "Ok";
        } catch (FileNotFoundException e) {
	        return ("Erro salvando arquivo: " +  e.getMessage());
        }
        
	}
}
