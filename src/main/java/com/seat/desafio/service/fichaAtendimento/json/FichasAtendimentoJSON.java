package com.seat.desafio.service.fichaAtendimento.json;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import com.seat.desafio.model.FichaAtendimento;
import com.seat.desafio.repository.FichasAtendimento;

public class FichasAtendimentoJSON implements FichasAtendimento {

	private RestTemplate restTemplate;
	
	@Override
	public FichaAtendimento buscarFichaDeAtendimento(String nome) {
		createRestTemplate();
		
		URI uri = new UriTemplate("http://seat.ind.br/processo-seletivo/2018/01/desafio.php?nome={0}").expand(nome);
		
		RequestEntity<Void> request = RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON_UTF8)
				.build();
		
		ResponseEntity<FichaAtendimento> response = restTemplate.exchange(request, FichaAtendimento.class);
		
		
		return response.getBody();
	}

	@Override
	public String enviarFichasAtendimento(FichaAtendimento fichaAtendimento) {
		 try {
	            String data = "nome=" + fichaAtendimento.getNome() + "&chave=" + fichaAtendimento.getChave() + "&resultado=" + 
	            		new JSONArray(fichaAtendimento.getResultados());
	            URL url = new URL(fichaAtendimento.getFichaAtendimentoEnvio().getUrl());
	            URLConnection conn = url.openConnection();
	            conn.setDoOutput(true);
	            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	            wr.write(data);
	            wr.flush();
	            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String line;
	            while ((line = rd.readLine()) != null) {
	                System.out.println(line);
	            }
	            wr.close();
	            rd.close();
	        } catch (Exception e) {
	        }
		
		return null;
		
	}
	
	private void createRestTemplate() {
		restTemplate = new RestTemplate();
		
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();        
	
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
	    supportedMediaTypes.addAll(converter.getSupportedMediaTypes());
	    supportedMediaTypes.add(new MediaType("text", "html", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
		
		converter.setSupportedMediaTypes(supportedMediaTypes);         
		messageConverters.add(converter);  
		restTemplate.setMessageConverters(messageConverters);  
	}
}
