package com.seat.desafio.service.fichaAtendimento;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.seat.desafio.model.Atendimento;

class CalculadoraAtendimento {
	
	public List<Long> calcularTempoMedioAtendimento(List<Atendimento> atendimentos) {
		double tempoMedioDeChegada = calcularTempoMedioDeChegada(atendimentos);
		double tempoMedioDeAtendimento = calcularTempoMedioDeAtendimento(atendimentos);
		return calcularValorTempoMedioDeEsperaNaFila(atendimentos, tempoMedioDeAtendimento, tempoMedioDeChegada);
	}
	
	public Map<Integer, Integer> calcularTempoDeEsperaPorFaixaParaImpressao(List<Atendimento> listaParaCalculo, 
			Map<Integer,Integer> tempoDeEsperaPorFaixa, Long tempoMinimoDeEspera, Integer verificador) {
		if (listaParaCalculo.isEmpty()) {
			return tempoDeEsperaPorFaixa;
		}
		
		int valor;
		for (int i = 0; i < listaParaCalculo.size(); i++) {
			if ( (listaParaCalculo.get(0).getTempoMediaEspera() / 1000 / 60) < verificador) {				
				valor = 1;
				if (tempoDeEsperaPorFaixa.containsKey(verificador)) {
					valor += tempoDeEsperaPorFaixa.get(verificador).intValue();
				}
				tempoDeEsperaPorFaixa.put(verificador, valor);
				listaParaCalculo.remove(i);
				i--;
			}
		}
		
		verificador += 5;
		
		return calcularTempoDeEsperaPorFaixaParaImpressao(listaParaCalculo, tempoDeEsperaPorFaixa, tempoMinimoDeEspera, verificador);
	}

	private List<Long> calcularValorTempoMedioDeEsperaNaFila(List<Atendimento> atendimentos, double tempoMedioDeAtendimento, double tempoMedioDeChegada) {
		List<Long> temposDeAtendimento = new ArrayList<>();
		double LAMBDA = 1 / tempoMedioDeChegada;
		double MI = 1 / tempoMedioDeAtendimento;
		double RO = LAMBDA / MI;
		double IIDeZero = 0;
		double LS = 0;
		double LQ = 0;
		double IIDeC = 0;
		double WQ = 0;
		for (int i = 0; i < atendimentos.size(); i++) {
			IIDeZero = calcularIIDeZero(i+1, RO);
			LS = 1 - IIDeZero;
			LQ = calcularValorDeL(i+1, RO) - LS;
			IIDeC = calcularIIDeC(i+1, RO, IIDeZero);
			WQ = (LQ) / (LAMBDA * (1 - IIDeC));
			
			atendimentos.get(i).setTempoMediaEspera((long) WQ);
			temposDeAtendimento.add((long) WQ);
		}
		return temposDeAtendimento;
	}

	private double calcularIIDeC(int capacidade, double RO, double IIDeZero) {
		return Math.pow(RO, capacidade) * IIDeZero;
	}

	private double calcularValorDeL(int capacidade, double RO) {
		return ( RO * (1 - (capacidade+1) * Math.pow(RO, capacidade) + capacidade * Math.pow(RO, capacidade+1)) ) / 
				( (1 - Math.pow(RO, capacidade+1)) * (1 - RO) );
	}
	
	private double calcularIIDeZero(int capacidade, double RO) {
		return (1 - RO) / (1- Math.pow(RO, capacidade+1));
	}

	private double calcularTempoMedioDeChegada(List<Atendimento> atendimentos) {
		List<Atendimento> listPraCalculo= new ArrayList<>();
		listPraCalculo.addAll(atendimentos.stream().filter(a -> a.getFim() != null).collect(Collectors.toList()));
		if (listPraCalculo.size() > 1) {
			return (listPraCalculo.get(listPraCalculo.size()-1).getEmissao() - listPraCalculo.get(0).getEmissao()) / (listPraCalculo.size()-1);
		} else {
			return 0;
		}
	}
	
	private double calcularTempoMedioDeAtendimento(List<Atendimento> atendimentos) {
		List<Atendimento> listPraCalculo= new ArrayList<>();
		listPraCalculo.addAll(atendimentos.stream().filter(a -> a.getFim() != null).collect(Collectors.toList()));
		
		if (listPraCalculo.size() > 1) { 
			return (listPraCalculo.get(listPraCalculo.size()-1).getChamada() - listPraCalculo.get(0).getChamada()) / (listPraCalculo.size());
		} else {
			return 0;
		}
	}
}
