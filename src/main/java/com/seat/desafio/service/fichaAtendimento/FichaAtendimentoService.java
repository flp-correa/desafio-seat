package com.seat.desafio.service.fichaAtendimento;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.seat.desafio.model.Atendimento;
import com.seat.desafio.model.FichaAtendimento;
import com.seat.desafio.repository.FichasAtendimento;

public abstract class FichaAtendimentoService {
	
	protected abstract FichasAtendimento criarFichasAtendimento();
	
	public FichaAtendimento buscarFichaDeAtendimento(String nome) {
		return criarFichasAtendimento().buscarFichaDeAtendimento(nome);
	}
	
	public void enviarFichaDeAtendimento(FichaAtendimento fichaAtendimento) {
		criarFichasAtendimento().enviarFichasAtendimento(fichaAtendimento);
	}
	
	public void executar (FichaAtendimento fichaAtendimento) {
		CalculadoraAtendimento calculadoraAtendimento =  new CalculadoraAtendimento();
		removerFichasDiplicadas(fichaAtendimento);
		fichaAtendimento.setAtendimentos(ordernarFichaAtendimentoPorEmissao(fichaAtendimento.getAtendimentos()));
		List<Long>	atendimentosComTempo = calculadoraAtendimento.calcularTempoMedioAtendimento(fichaAtendimento.getAtendimentos());
		removerAtendidosParaResultadoFinal(fichaAtendimento);
		ordenarListaDeEsperaPorPrioridadeEEmissao(fichaAtendimento.getResultados());
		redefinirTempoDeAtendimentos(fichaAtendimento.getResultados(), atendimentosComTempo);
	}
	
	public Map<Integer, Integer> calcularTempoDeEsperaPorFaixaParaImpressao(List<Atendimento> listaParaCalculo, 
				Map<Integer,Integer> tempoDeEsperaPorFaixa, Long tempoMinimoDeEspera, Integer verificador) {
		return new CalculadoraAtendimento().calcularTempoDeEsperaPorFaixaParaImpressao(listaParaCalculo, tempoDeEsperaPorFaixa, tempoMinimoDeEspera, verificador);
	}

	private void redefinirTempoDeAtendimentos(List<Atendimento> atendimentos, List<Long> atendimentosComTempo) {
		int cont = atendimentosComTempo.size()-1;
		for (int i = atendimentos.size()-1; i >= 0  ; i--) {
			atendimentos.get(i).setTempoMediaEspera(atendimentosComTempo.get(cont));
			atendimentos.get(i).setNaFrente(i);
			cont--;
		}
	}

	private void removerFichasDiplicadas(FichaAtendimento fichaAtendimento) {		
		fichaAtendimento.setAtendimentos(fichaAtendimento.getAtendimentos().stream().distinct().collect(Collectors.toList()));
	}
	
	private List<Atendimento> ordernarFichaAtendimentoPorEmissao(List<Atendimento> atendimentos) {
		Comparator<Atendimento> comparator = Comparator.comparing(Atendimento::getEmissao);
		
		return atendimentos.stream().sorted(comparator).collect(Collectors.toList());
	}
	
	private void removerAtendidosParaResultadoFinal(FichaAtendimento fichaAtendimento) {
		fichaAtendimento.getResultados().addAll(fichaAtendimento.getAtendimentos());
		fichaAtendimento.getResultados().removeIf(a -> a.getChamada() != null);
	}
	
	private void ordenarListaDeEsperaPorPrioridadeEEmissao(List<Atendimento> atendimentos) {		
		Collections.sort(atendimentos);
	}
}
