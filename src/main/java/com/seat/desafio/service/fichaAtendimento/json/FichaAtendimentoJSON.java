package com.seat.desafio.service.fichaAtendimento.json;

import com.seat.desafio.repository.FichasAtendimento;
import com.seat.desafio.service.fichaAtendimento.FichaAtendimentoService;

public class FichaAtendimentoJSON extends FichaAtendimentoService {

	@Override
	protected FichasAtendimento criarFichasAtendimento() {
		return new FichasAtendimentoJSON();
	}
}
