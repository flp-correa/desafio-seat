package com.seat.desafio.service.fichaAtendimento.xml;
import com.seat.desafio.repository.FichasAtendimento;
import com.seat.desafio.service.fichaAtendimento.FichaAtendimentoService;

public class FichaAtendimentoXML extends FichaAtendimentoService {

	@Override
	protected FichasAtendimento criarFichasAtendimento() {
		return new FichasAtendimentoXML();
	}
}
