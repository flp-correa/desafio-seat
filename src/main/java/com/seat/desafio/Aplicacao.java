package com.seat.desafio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.seat.desafio.model.Atendimento;
import com.seat.desafio.model.FichaAtendimento;
import com.seat.desafio.service.fichaAtendimento.FichaAtendimentoService;
import com.seat.desafio.service.fichaAtendimento.json.FichaAtendimentoJSON;

public class Aplicacao {

	public static void main(String[] args) {
		//FichaAtendimentoService fichaAtendimentoXML = new FichaAtendimentoXML();
		FichaAtendimentoService fichaAtendimentoJSON = new FichaAtendimentoJSON();
		
		FichaAtendimento ficha = fichaAtendimentoJSON.buscarFichaDeAtendimento("Felipe Correa");
		fichaAtendimentoJSON.executar(ficha);
		
		System.out.println(ficha.getChave() + " - " + ficha.getMensagem() + " - " + 
		ficha.getFichaAtendimentoEnvio().getUrl() + " - " + ficha.getFichaAtendimentoEnvio().getParameters());
		
		fichaAtendimentoJSON.enviarFichaDeAtendimento(ficha);
		imprimirHistogramaComTempoDeEspera(ficha.getAtendimentos(), fichaAtendimentoJSON);
		
	}
	
	public static void imprimirHistogramaComTempoDeEspera(List<Atendimento> atendimentos, FichaAtendimentoService fichaAtendimento) {
		List<Atendimento> listaParaCalculo = new ArrayList<>();
		listaParaCalculo.addAll(atendimentos);
		Long tempoMinimoDeEspera = listaParaCalculo.get(0).getTempoMediaEspera() / 1000 / 60;
		Map<Integer, Integer> tempoDeEsperaPorFaixa = fichaAtendimento.calcularTempoDeEsperaPorFaixaParaImpressao(listaParaCalculo, new HashMap<>(), tempoMinimoDeEspera, 5);
		
		Map<Integer, Integer> resultOrdenado = tempoDeEsperaPorFaixa.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		
		for (Integer chave : resultOrdenado.keySet()) {
			System.out.println("< " + chave + "min: " +  
					new String(new char[resultOrdenado.get(chave).intValue()]).replace("\0", "#") + " " +  resultOrdenado.get(chave).intValue() );
		}
	}
	
	
}
