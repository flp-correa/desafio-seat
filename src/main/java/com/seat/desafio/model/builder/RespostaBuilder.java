package com.seat.desafio.model.builder;

import java.util.List;

import com.seat.desafio.model.Atendimento;
import com.seat.desafio.model.Resposta;

public class RespostaBuilder {

	private Resposta instancia;
	
	public RespostaBuilder() {
		instancia = new Resposta();
	}
	
	public RespostaBuilder comNome(String nome) {
		instancia.setNome(nome);
		return this;
	}
	
	public RespostaBuilder comChave(String chave) {
		instancia.setChave(chave);
		return this;
	}
	
	public RespostaBuilder comResultado(List<Atendimento> resultado) {
		instancia.setResultado(resultado);
		return this;
	}
	
	public Resposta construir() {
		return instancia;
	}
}
