package com.seat.desafio.model;

import java.util.List;

public class FichaAtendimentoEnvio {

	private String url;
	private List<String> parameters;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<String> getParameters() {
		return parameters;
	}
	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}
}
