package com.seat.desafio.model;

import java.time.Instant;
import java.time.ZoneId;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Atendimento implements Comparable<Atendimento> {

	private String senha;
	private Long emissao;
	
	@JsonDeserialize
	private PrioridadeAtendimento prioridade;
	private Long chamada;
	private String guiche;
	private String atendente;
	private Long fim;
	private Integer naFrente;
	
	@JsonProperty("espera")
	private Long tempoMediaEspera;

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Long getEmissao() {
		return emissao;
	}

	public void setEmissao(Long emissao) {
		this.emissao = emissao;
	}

	public PrioridadeAtendimento getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(PrioridadeAtendimento prioridade) {
		this.prioridade = prioridade;
	}

	public Long getChamada() {
		return chamada;
	}

	public void setChamada(Long chamada) {
		this.chamada = chamada;
	}

	public String getGuiche() {
		return guiche;
	}

	public void setGuiche(String guiche) {
		this.guiche = guiche;
	}

	public String getAtendente() {
		return atendente;
	}

	public void setAtendente(String atendente) {
		this.atendente = atendente;
	}

	public Long getFim() {
		return fim;
	}

	public void setFim(Long fim) {
		this.fim = fim;
	}

	public Long getTempoMediaEspera() {
		return tempoMediaEspera;
	}

	public void setTempoMediaEspera(Long tempoMediaEspera) {
		this.tempoMediaEspera = tempoMediaEspera;
	}
	
	@Override
	public String toString() {
		return "Atendimento [senha=" + senha + ", emissao=" +  Instant.ofEpochMilli(emissao).atZone(ZoneId.systemDefault()).toLocalDateTime() +
				", prioridade=" + prioridade + ", chamada="
				+ chamada + ", naFrente=" + naFrente + ", tempoMediaEspera=" + tempoMediaEspera + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atendimento other = (Atendimento) obj;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		return true;
	}

	public Integer getNaFrente() {
		return naFrente;
	}

	public void setNaFrente(Integer naFrente) {
		this.naFrente = naFrente;
	}

	@Override
	public int compareTo(Atendimento o) {
		if (this.prioridade.equals(PrioridadeAtendimento.preferencial) && o.prioridade.equals(PrioridadeAtendimento.preferencial) &&
				this.emissao < o.emissao) {
			return -1;
		} 
		
		if (this.prioridade.equals(PrioridadeAtendimento.preferencial) && o.prioridade.equals(PrioridadeAtendimento.geral)) {
			return -1;
		}
		
		if (this.emissao < o.emissao) {
			return -1;
		}
		if (this.emissao > o.emissao) {
			return 1;
		}
		return 0;
	}
}
