package com.seat.desafio.model;

public enum PrioridadeAtendimento {

	preferencial("Preferencial"),
	geral("Geral");
	
	private String descricao;

	private PrioridadeAtendimento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
