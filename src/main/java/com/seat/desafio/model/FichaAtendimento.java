package com.seat.desafio.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FichaAtendimento {

	private String mensagem;

	private String nome;
	
	private String chave;
	
	@JsonProperty("input")
	private List<Atendimento> atendimentos = new ArrayList<>();
	
	@JsonProperty("resultado")
	private List<Atendimento> resultados = new ArrayList<>();
	
	@JsonProperty("postTo")
	private FichaAtendimentoEnvio fichaAtendimentoEnvio;
	
	@JsonProperty("parameters")
	private Resposta resposta;
	
	private double tempoMedioDeChegada;
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public List<Atendimento> getAtendimentos() {
		return atendimentos;
	}

	public void setAtendimentos(List<Atendimento> atendimentos) {
		this.atendimentos = atendimentos;
	}

	public List<Atendimento> getResultados() {
		return resultados;
	}

	public void setResultados(List<Atendimento> resultados) {
		this.resultados = resultados;
	}

	public FichaAtendimentoEnvio getFichaAtendimentoEnvio() {
		return fichaAtendimentoEnvio;
	}

	public void setFichaAtendimentoEnvio(FichaAtendimentoEnvio fichaAtendimentoEnvio) {
		this.fichaAtendimentoEnvio = fichaAtendimentoEnvio;
	}

	public double getTempoMedioDeChegada() {
		return tempoMedioDeChegada;
	}

	public void setTempoMedioDeChegada(double tempoMedioDeChegada) {
		this.tempoMedioDeChegada = tempoMedioDeChegada;
	}

	public Resposta getResposta() {
		return resposta;
	}

	public void setResposta(Resposta resposta) {
		this.resposta = resposta;
	}
}
